# SOFTWARE UPDATE
GITLAB_PROJECT_ID = 7970541
LATEST_VERSION_URL = "https://gitlab.com/api/v4/projects/%s/releases" % GITLAB_PROJECT_ID
PLUGIN_UPDATE_URL = "https://gitlab.com/mosaic-mfg/OctoPrint-Netconnectd/-/archive/master/OctoPrint-Netconnectd-master.zip"

# coding=utf-8
import setuptools
import octoprint_setuptools

setuptools.setup(**octoprint_setuptools.create_plugin_setup_parameters(
    identifier="netconnectd",
    name="OctoPrint-Netconnectd",
    version="1.1.0",
    description="Slightly modified client for netconnectd that allows configuration of netconnectd through OctoPrint's settings dialog. It's only available for Linux right now.",
    author="Gina Häußge, Mosaic Manufacturing Ltd.",
    mail="info@mosaicmfg.com",
    url="https://gitlab.com/mosaic-mfg/OctoPrint-Netconnectd",
    requires=[
                "OctoPrint"
    ]
))
